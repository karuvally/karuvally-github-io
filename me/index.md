---
layout: about
---

Hi, I am Aswin. Lover of computers, *nix, autos and all things tech. I program, 
read, teach and does some basic I/O, when bored. This is my memory dump for all good people to see :)

# What do you do?
I tweak my Linux box, try to get better at programming, lust at old hardware and cars and spends a good deal of time thinking. I am also trying to earn masters in computer application.

# What programming languages do you use?
Anything that makes me happy :D (Though Python 3 and C are favorites) 

# Which OS do you use?
Lots and lots of Linux with Windows thrown in occasionaly

# Who designed this blog
The blog was designed by a person called [Micah Cowell](http://blog.micahcowell.com) who was nice enough to publish the template under MIT license...

# Can you be hired?
Yes, ofcourse! take a look at my [resume](https://github.com/karuvally/cv/raw/master/resume_karuvally.pdf)

# Contact
Feel free to drop in a mail: aswinbabuk [at] gmail [dot] com  
Better still, you can connect with me on [LinkedIn](https://in.linkedin.com/in/karuvally)
